/*
 * File: shell.nix
 * Project: 
 * Created Date: Tuesday October 26th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 10th October 2022 5:04:24 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */
with import <nixpkgs> {};

let
    pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "pinned_nix_packages";                                                 
         url = "https://github.com/nixos/nixpkgs/";  
         ref = "nixos-22.05";                     
         rev = "cf63ade6f74bbc9d2a017290f1b2e33e8fbfa70a";                                           
     }) {};    

    python = pkgs.python310;
    dontCheckPython = drv: drv.overridePythonAttrs (old: { doCheck = false; });
    python_pkgs = (python.withPackages (p: with p; [
      pyserial
      termcolor
      pyelftools
      pint
      shapely
      jsonpickle
      (dontCheckPython numpy)
      (dontCheckPython matplotlib)
    ])).override (args: { ignoreCollisions = true; });
in 

 # Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "app_maker";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = with pkgs; [
    libusb1
    libftdi1
    python_pkgs
    openocd
  ];
}