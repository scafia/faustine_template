
import sys, os

# sys.path.insert(0, os.path.abspath('..'))
sys.path.insert(0, os.path.abspath('../benches/code/'))

from aes import encrypt
import secrets
import matplotlib.pyplot as plt
import numpy as np

from stm32 import STM32
from helpers.openocd_cortexm3 import OpenOCD
from stages.xyz_stage import XYZStage, AreaOfInterest
from pint import Quantity, UnitRegistry
from generators.AVRK4 import AVRK4
from shapely.geometry import MultiPoint, Point, Polygon

from os import listdir
from os.path import isfile, join
import serial
from termcolor import colored, cprint
from time import sleep

def read_whole_file(path):
    buffer = bytearray()
    with open(path, "rb") as f:
        byte = f.read(1)
        while byte != b"":
            buffer.append(byte[0])
            byte = f.read(1)

    # print(buffer.hex())
    return bytes(buffer)

# find ports for stm
def find_stm_port():

    ttyUSBX = ["/dev/"+f for f in listdir("/dev") if "ttyUSB" in f]
    # print(ttyUSBX)

    stm_ttyUSB = None
    

    # find stm

    for ttyUSB in ttyUSBX:
            stm_candidate = STM32(ttyUSB)
            test = stm_candidate.test()
            stm_candidate.close()
            
            if test == True:
                stm_ttyUSB = ttyUSB
                break


    return stm_ttyUSB

def find_stage_port():
    ttyUSBX = ["/dev/"+f for f in listdir("/dev") if "ttyUSB" in f]

    for ttyUSB in ttyUSBX:
            stage = XYZStage(ttyUSB)
            if stage.test() == True:
                return ttyUSB

    return None


if __name__ == "__main__":

    ureg = UnitRegistry()

    stm_tty = find_stm_port()
    print("STM found at", stm_tty)

    stage_tty = find_stage_port()
    print("Stage found at", stage_tty)

    # stm_tty = "/dev/ttyUSB2"
    # stage_tty = "/dev/ttyUSB0"

    stm = STM32(stm_tty)
    stage = XYZStage(stage_tty)
    
    # Connect to OpenOCD, STM32 JTAG
    ocd = OpenOCD(["./targeted_program/stm32vldiscovery.cfg"])


    # Init procedure
    ocd.reset()

    # Welcome message
    welcome_msg = stm.read_bytes(22)
    assert welcome_msg == b"Shall we play a game?\n", "Welcome message incorrect: " + str(welcome_msg)

    # Test connection
    stm.test()

    # Connect to generator
    avrk4 = AVRK4()
    print("Connected to AVRK-4")

    # Init stage
    area = AreaOfInterest.load("area")
    stage.homing()
    focus = Polygon([[-2.5, 5.5], [-2.5, 6.5], [-1.5, 6.5], [-1.5, 5.5] ])
    points = stage.getSurfaceIterator(area.focus(focus), Quantity(0.5, ureg.mm))


    # key = b"0123456789ABCDEF"
    key = bytearray([0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c])
    stm.set_key(key)
    stm.set_round(10)

    cts = []
    cts_ok = []

    tries_per_point = 20

    avrk4.set_ext_trig()

    amplitude = Quantity(700, ureg.volt)
    delays = range(2700,3500,1)

    avrk4.set_amplitude(amplitude)

    avrk4.activate()
    for point in points:
        stage.moveXYZ_dimless(point)
        print(point)

        for d in delays:
            delay_ns = Quantity(d, ureg.ns)
            avrk4.set_trigger_delay(delay_ns)
            

            for i in range(tries_per_point):
                avrk4.activate()
                # sleep(0.05) # rate limit
                pt = secrets.token_bytes(16)
                # pt = bytearray([0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34])
                ptarr = np.frombuffer(pt, dtype=np.uint8)
                ct = stm.encrypt(pt)
                if len(ct) != 16:
                    cprint("\tCrash! @" + str(d), 'red')
                    continue
                ctarr = np.frombuffer(ct, dtype=np.uint8)
                ct_ok = encrypt(pt, key)
                ct_okarr = np.frombuffer(ct_ok, dtype=np.uint8)

                diff = np.bitwise_xor(ctarr, ct_okarr)
                diffstr = ''.join(format(x, '02x') for x in diff)

                if np.array_equal(diff, np.zeros(16, dtype=np.uint8)) == False:
                    cprint("\t" + str(d) + " -> " + diffstr + " @" + str(d), 'red')
                    cts.append(ctarr)
                    cts_ok.append(ct_okarr)
                # else:
                #     cprint(diffstr, 'green')

    # print("Starting...")
    # for i in range(1000):
    #     pt = secrets.token_bytes(16)
    #     # pt = bytearray([0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34])
    #     ptarr = np.frombuffer(pt, dtype=np.uint8)
    #     ct = stm.encrypt(pt)

    avrk4.shutdown()

    np.save("ctsf.npy", np.array(cts))
    np.save("cts.npy", np.array(cts_ok))