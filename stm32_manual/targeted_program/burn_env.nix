/*
 * File: buen_env.nix
 * Project: 
 * Created Date: Tuesday October 26th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 10th October 2022 5:04:24 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */
with import <nixpkgs> {};

let
  pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "pinned_nix_packages";                                                 
         url = "https://github.com/nixos/nixpkgs/";                       
         rev = "e741da0bc838a40f81ff48c0eb491932468cc563";                                           
     }) {}; 
in 

 # Make a new "derivation" that represents our shell
stdenv.mkDerivation {
  name = "app_maker";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = with pkgs; [
    gcc-arm-embedded
    openocd
    libusb1
    libftdi1
    which
  ];
}