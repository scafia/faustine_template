#!/usr/bin/env nix-shell
#!nix-shell burn_env.nix -i bash

sudo modprobe -r uas
sudo modprobe -r usb-storage
sudo modprobe usb-storage quirks=483:3744:i
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

if [ -z $1 ]; then
$(which openocd) -f $SCRIPTPATH/stm32vldiscovery.cfg -c "program $SCRIPTPATH/aes.elf verify reset exit"
else
$(which openocd) -f $SCRIPTPATH/stm32vldiscovery.cfg -c "program $1 verify reset exit"
fi
