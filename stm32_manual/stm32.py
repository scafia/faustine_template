'''
File: app.py
Project: experiments
Created Date: Thursday December 10th 2020
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Wednesday, 29th June 2022 11:10:58 am
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2020 INRIA
'''

import serial
import time
from termcolor import colored, cprint

def bytes_from_size(sz):
    buffer = bytearray()
    buffer.append(sz >> 8 & 0xff)
    buffer.append(sz & 0xff)
    return bytes(buffer)

def size_from_bytes(sz_bytes):
    return sz_bytes[1] + (sz_bytes[0] << 8)


class STM32:

    BAUD_RATE = 115200

    def __init__(self, serial_port, debug=False, timeout=3):
        self.serial_handle = serial.Serial(serial_port, self.BAUD_RATE, timeout=timeout)
        self.debug = debug
        self.timeout = timeout

    def close(self):
        self.serial_handle.close()

    # RAW SERIAL

    def send_bytes(self, bytes):
        self.serial_handle.write(bytes)

    def send_str(self, str):
        self.serial_handle.write(str.encode('utf-8'))

    def read_bytes(self, count) -> bytes:
        return self.serial_handle.read(count)

    def read_until(self, end_bytes):
        buffer = bytearray()

        start = time.time()
        
        while buffer.endswith(end_bytes) == False:
            now = time.time()
            if now-start > self.timeout:
                break
            buffer.extend(self.read_bytes(1))

        return bytes(buffer)

    

    # Higher level API

    def debug_read(self):
        while True:
            buffer = self.read_bytes(1)
            print("%c" %buffer[0], end="", flush=True)

    def wait_for_ack_or_nack(self, errorMessage=""):
        buf = self.read_until(b"\r\n")

        if buf.startswith(b"nack"):
            cprint(errorMessage + ": " + buf.decode("utf-8").strip(), 'red')
        elif self.debug:
            cprint("ack", 'green')

        return buf

    def test(self):
        self.send_bytes(b"t")
        if self.debug:
            cprint("Sending test command", 'yellow', end="... ")
        ans = self.wait_for_ack_or_nack(errorMessage="Connection test failed")
        return ans == b"ack\r\n"

    
    def set_key(self, key_bytes: bytes):
        assert len(key_bytes) == 16
        self.send_bytes(b"k" + key_bytes)
        self.wait_for_ack_or_nack(errorMessage="Connection test failed")

    def set_round(self, round: int):
        self.send_bytes(b"r" + (round.to_bytes(1, byteorder='big')))
        self.wait_for_ack_or_nack(errorMessage="Connection test failed")
    
    def encrypt(self, plaintext: bytes) -> bytes:
        assert len(plaintext) == 16
        self.send_bytes(b"f" + plaintext)
        ciphertext = self.read_bytes(16)
        return ciphertext